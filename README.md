# Numeric Tolerances


The input files `weatherA.xml` and `weatherB.xml`illustrate numeric differences in elements and attributes.  The `numeric` option can be used to specify the tolerance that is allowed for a particular element or attribute between the two input files. 

When an empty configuration file is specified, then the time attribute and all the temperatures will be reported as different.  The config file `config-temp-numeric.xml` allows different tolerances.


## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/weatherA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/weatherB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-temp-numeric.xml</path>
    </configuration>
</compare>
```


See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.

Once you have downloaded the sample and set them to run in REST, it becomes very easy to experiment with different XPaths and values to match your data.  See the web page [Numeric Tolerances](https://docs.deltaxml.com/xdc/latest/samples/numeric-tolerances) for a discussion of this sample.
